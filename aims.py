import numpy as np

def std(x):
    x_bar = np.mean(x)
    devs=0.0
    for i in range(len(x)):
        devi = (x[i] - x_bar)**2
        devs += devi
    return np.sqrt(devs/len(x))

def avg_range(lst):
    files=[]
    ranges=[]
    for location in lst:
        files.append(open(location))
    for entry in files:
        for line in entry:
            if 'Range' in line:
                ranges.append(float(line[7]))
    return sum(ranges)/len(ranges)


if __name__=='__main__':   
    print "Welcome to the AIMS module"

