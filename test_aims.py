from nose.tools import assert_equal

import aims

def test_ints():
    numbers = [45,45]
    obs = aims.std(numbers)
    exp = 0
    assert_equal(obs,exp)


def test_double():
    numbers = [-4,-5]
    obs = aims.std(numbers)
    exp = 0.5
    assert_equal(obs,exp)


def test_negaints():
    numbers = [-2,-2]
    obs = aims.std(numbers)
    exp = 0
    assert_equal(obs,exp)



def test_negapositve():
    numbers = [-2,4]
    obs = aims.std(numbers)
    exp = 3
    assert_equal(obs,exp)


def test_average_data():
	fil_1 = ['data/bert/audioresult-00319']
	obs = aims.avg_range(['data/bert/audioresult-00319'])
	exp = 10
	assert_equal(obs,exp)


def test_average_data():
	fil_1 = ['data/bert/audioresult-00380']
	obs = aims.avg_range(['data/bert/audioresult-00380'])
	exp = 9
	assert_equal(obs,exp)


def test_average_data():
	fil_1 = ['data/bert/audioresult-00493']
	obs = aims.avg_range(['data/bert/audioresult-00493'])
	exp = 9
	assert_equal(obs,exp)


